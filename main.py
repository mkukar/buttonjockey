# Button Jockey
# For Alexa w. Button
# Horse race rythym game
# Copyright Michael Kukar 2018. All Rights Reserved.

import sys
from threading import Thread

from Horse import Horse
from Announcer import Announcer

#
# GLOBAL VARIABLES
#

MAX_PLAYERS = 4
MIN_PLAYERS = 1
TRACK_LENGTH = 15
TOTAL_PLAYERS = 8

version = 0.1
'''
STATES:
    Start
    Race
    End
'''
state = "START"

# 
# MAIN
#

if __name__ == "__main__":
    print("Horse Race v" + str(version))
    print("CB: Michael Kukar 2018")
    
    while(True):
    
        # boot up state machine
        if state == "START":
            # START
            # select players
            # pass into next state -> RACE
            # set state to RACE
            playerCount = 0
            while playerCount < MIN_PLAYERS or playerCount > MAX_PLAYERS:
                playerInput = input("How many players? (1-4):")
                try:
                    playerCount = int(playerInput)
                except Exception as e:
                    print("Invalid player count.")
            print(str(playerCount) + " player game selected.")
            state = "RACE"
            
        elif state == "RACE":
            # RACE
            # starts race with announcer and game manager
            # keeps track of players location
            # when a player crosses the finish line, the race ends
            # set state to END
            '''
            8 horses
            1 - 4 players
            each horse moves 1, 2 or 3 depending on execution (randomize for CPU players, fixed for normal)
            each "tick" the announcer says something and everyone makes their move (while the announcer is talking)
            the track is x units long, once that is reached we are done
            think of the arcade game where you roll the balls
            '''
            gameOver = False
            winner = 0
            # instantiate the number of cpu and human players
            # NOTE - player ID is horse - 1 (start at 0)
            horses = []
            horseThreads = []
            horseLocs = [0 for x in range(TOTAL_PLAYERS)]
            for x in range(playerCount):
                horses.append(Horse(True))
            for x in range(TOTAL_PLAYERS - playerCount):
                horses.append(Horse(False))
            # instantiate the announcer
            announcer = Announcer(TOTAL_PLAYERS, playerCount)
            
            # announcer makes opening remarks
            announcer.openingRemarks()
            
            while not gameOver:
                # everyone makes their move threading
                # spawns threads only for players so they don't have to wait for each other to move
                for horseNum in range(playerCount):
                    horseThreads.append(Thread(target = horses[horseNum].run, args=(horseLocs, horseNum)))
                    horseThreads[-1].start()
                    horseThreads[-1].join()
                for horseNum in range(len(horses)-playerCount):
                    horses[horseNum+playerCount].run(horseLocs, horseNum+playerCount)
                for horseNum in range(len(horses)):
                    if horseLocs[horseNum] >= TRACK_LENGTH:
                        winner = horseNum
                        gameOver = True
                        break
                # once a move completes, check if crossed finish line for gameOver
                # the announcer talks throughout by polling results as they come in
                announcer.commentate(horseLocs)

            # announcer says the winner and closing statement
            announcer.closingRemarks(winner)
            state = "END"
            
        elif state == "END":
            if input("Would you like to play again? (y/n):") == "y":
                state = "START"
            else:
                print("Thanks for playing!")
                sys.exit(0) 

