# Announcer.py
# Handles the announcer who will narrate the entire race
# Copyright Michael Kukar 2018. All Rights Reserved.

import sys, time

class Announcer:
    
    # VARIABLES
    horseCount = 8 # default
    humanCount = 1 # we favor narrating humans over computers
    
    # CONSTRUCTOR
    # takes in number of horses and humans as input
    # param horseCount : int value of total number of horses
    # param humanCount : int value of total number of human players
    def __init__(self, horseCount, humanCount):
        self.horseCount = horseCount
        self.humanCount = humanCount
    
    # FUNCTIONS
    
    # speaks the words of the announcer
    # param words : words to speak out loud
    # return      : none
    def speak(self, words):
        # TODO - replace with Alexa command
        for letter in words:
            sys.stdout.write(letter)
            sys.stdout.flush()
            time.sleep(0.04)    
        sys.stdout.write("\n")
        sys.stdout.flush()

    
    # makes opening remarks before the race begins
    # return : none
    def openingRemarks(self):
        openingStatement = "The race is about to begin! We have a total of " + str(self.horseCount) + " horses racing today. "
        for player in range(self.humanCount):
            openingStatement += "In stall number " + str(player+1) + " we have player " + str(player+1) + "! "
        if (self.humanCount < self.horseCount):
            openingStatement += "All other horses will be jockeyed by robots powered by Amazon. "
        openingStatement += "Everything is set...Music plays..."
        openingStatement += "And they're off!"
        self.speak(openingStatement)
        
    # makes closing remarks based on what horse won
    # param winningHorse : int value of winning horse number from 1 - horseCount
    # return             : none
    def closingRemarks(self, winningHorse):
        closingStatement = "Photo finish! And the winner is...Number " + str(winningHorse+1) + " jockeyed by " 
        # human player won
        if (winningHorse < self.humanCount):
            closingStatement += "Player " + str(winningHorse+1) + "! Hope to see you all at the races again soon!"
        # CPU won
        else:
            closingStatement += "Amazon's robotic horse jockey number " + str(winningHorse * 17267) + ". Better luck next time!"
        self.speak(closingStatement)
        
    # makes commentary during the race based on how the horses are performing
    # param horseLocs : list of each horses location in order from 0 to horseCount-1
    # return          : none
    def commentate(self, horseLocs):
        # basic version just say who is in the lead
        firstPlace = 0
        maxDist = max(horseLocs)
        for horseNum in range(len(horseLocs)):
            if horseLocs[horseNum] == maxDist:
                firstPlace = horseNum
                break
        
        commentary = "It's " + str(firstPlace+1) + " leading the pack."
        self.speak(commentary)
        
