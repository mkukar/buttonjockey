# Horse.py
# Handles a player or CPU controlled horse
# Copyright Michael Kukar 2018. All Rights Reserved.

import random
import msvcrt, time
import sys, os

class Horse:
    
    # VARIABLES
    isHuman = False
    curLoc = 0
    SLOW_STRIDE = 1
    MED_STRIDE = 2
    FAST_STRIDE = 3
    TIMESTEP = 0.7

    BUTTON_PATTERNS = [
        [1,0,0,1],
        [1,1,0,1],
        [1,0,1,1],
        [1,1,1,0],
        [0,0,1,1],
        [1,0,1,0],
        [0,1,1,0]
    ]
    
    # CONSTRUCTOR
    
    # creates the horse object as a human or computer player
    # param isHuman : boolean, if true is human player else is CPU
    def __init__(self, isHuman):
        self.isHuman = isHuman
    
    # GETTER/SETTERS
    
    # returns current location
    def getLocation(self):
        return self.curLoc
    
    # FUNCTIONS
    
    # main function, determines how far to move horse
    # if CPU, randomly generates result
    # if human, plays button pattern game to get results
    # locArr : array to store value in
    # locI   : index to store value in given locArr
    # return : none, mutates input value
    def run(self, locArr, locI):
        # human plays rythym game
        if self.isHuman:
            # randomizes sequence to use from list
            sequenceI = random.randint(0, len(self.BUTTON_PATTERNS)-1)
            self.curLoc += self.runGame(self.BUTTON_PATTERNS[sequenceI])        
        # computer gets randomized value
        else:
            randVal = random.randint(0,2)
            if randVal == 2:
                self.curLoc += self.FAST_STRIDE
            elif randVal == 1:
                self.curLoc += self.MED_STRIDE
            else:
                self.curLoc += self.SLOW_STRIDE
        locArr[locI] = self.getLocation()
    
    # plays the run game (takes a fixed amount of time)
    def runGame(self, sequenceArr = [1,0,1,0]):
        # TODO - replace with alexa-specific commands
        score = 0
        print()
        for sequence in sequenceArr:
            if sequence == 1:
                sys.stdout.write("\rPRESS BUTTON!")
            else:
                sys.stdout.write("\rWAIT!        ")
            buttonPressed = False
            start = time.time()
            while True:
                if msvcrt.kbhit():
                    msvcrt.getch()
                    buttonPressed = True
                    if sequence == 1:
                        sys.stdout.write("\rGOOD JOB!         ")
                    else:
                        sys.stdout.write("\rBAD TIMING!       ")
                if time.time() - start > self.TIMESTEP:
                    break
            if buttonPressed:
                if sequence == 1:
                    score += 1
                else:
                    score -= 1
            else:
                if sequence == 1:
                    sys.stdout.write("\rTOO SLOW!           ")
                    score -= 1
                else:
                    sys.stdout.write("\rGOOD JOB!          ")
                    score += 1
        # clears line
        sys.stdout.write("\r                          \r")
        # determines score
        if score == 4:
            return self.FAST_STRIDE
        elif score < 0:
            return self.SLOW_STRIDE
        else:
            return self.MED_STRIDE
