# signal_test.py
# tests out signals and interrupts, in this case focused on the keyboard
# Copyright Michael Kukar 2018. All Rights Reserved.

import threading
from threading import Timer
import msvcrt, time
import sys, os

score = 0
shouldPress = False
timedOut = False
WAIT_TIME = 1

def button_press_handler(isBad):
    global score, shouldPress
    if not isBad:
        print("Good Job!")
        score += 1
    else:
        print("Too slow!")
        score -= 1

def exceptionThrower():
    global timedOut
    timedOut = True

if __name__ == "__main__":
    print("signal test")
    while True:
        start = time.time()
        # HIT BUTTON CASE
        print("PRESS A BUTTON!")
        buttonPressed = False
        while True:
            if msvcrt.kbhit():
                msvcrt.getch()
                buttonPressed = True
            if time.time() - start > WAIT_TIME:
                break
        if buttonPressed:
            print("GOOD JOB!")
        else:
            print("TOO SLOW!")


        # reverse situation
        print("WAIT!")
        buttonPressed = False
        start = time.time()
        while True:
            if msvcrt.kbhit():
                msvcrt.getch()
                buttonPressed = True
            if time.time() - start > WAIT_TIME:
                break
        if buttonPressed:
            print("OUCH!")
        else:
            print("GOOD JOB!")
